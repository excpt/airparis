# airparis

### require
 - python 3+

### setup
`python setup.py install`

### usage
 - `python -m airparis.airparis`
 - open `http://127.0.0.1:8080/` in your browser
