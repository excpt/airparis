from flask import render_template
from geopy.distance import vincenty
import requests
from flask import Flask

app = Flask(__name__)
session = requests.Session()

PARIS_COORDINATES = (48.864716, 2.349014)


def get_states():
    response = session.get('https://opensky-network.org/api/states/all')
    result = []
    for state in response.json()['states']:
        distance = vincenty(PARIS_COORDINATES, (state[6], state[5])).km
        if ((state[6], state[5]) != (None, None)) and distance <= 450:
            result.append([state[1], state[6], state[5]])

    return result


@app.route('/')
def index_handler():
    return render_template('index.html', items=get_states())


if __name__ == '__main__':
    app.run('127.0.0.1', 8080)
