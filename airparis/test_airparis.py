from unittest import mock
import responses
from airparis import get_states, app


opensky_response = {
    'states': [
        ['a1', 'A1', None, None, None, 25.33, -94.9375],
        ['a2', 'A2', None, None, None, 7.56, 49.4615],
        ['a3', 'A3', None, None, None, 44.864716, 2.2349014],
        ['a4', 'A4', None, None, None, None, None],
    ]
}

@responses.activate
def test_get_states():
    responses.add(responses.GET, 'https://opensky-network.org/api/states/all',
                  json=opensky_response, status=200)
    result = get_states()
    assert result, [['A2', 49.4615, 7.56], ['A3', 48.864716, 2.349014]]


@mock.patch('airparis.get_states', return_value=[['AIR12345', 47.4615, 8.56]])
def test_index_handler(get_states_mock):
    client = app.test_client()
    response = client.get('/')
    assert b'AIR12345' in response.data
    assert b'47.4615' in response.data
    assert b'8.56' in response.data
    assert get_states_mock.called, True
