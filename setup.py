try:
    from setuptools import setup
except ImportError:
    from distutils import setup


REQUIREMENTS = [
    'geopy == 1.11.0',
    'flask == 0.12.2',
    'responses == 0.8.1',
    'requests == 2.18.4',
    'pytest',
]

setup(
    name="airparis",
    version="1.0.0",
    description="Getting aircrafts near Paris",
    author='excpt',
    packages=['airparis'],
    zip_safe=False,
    install_requires=REQUIREMENTS,
    include_package_data=True,
    package_data={  # Optional
        'airparis': ['templates/index.html'],
}
)